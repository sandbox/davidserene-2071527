<?php

class OAMTermOrgRole extends DynamicMigration {
  public function __construct($arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports terms of type Organisation Role.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = array(
      0 => array('code', 'code'),
      1 => array('name', 'name'),
      2 => array('description', 'description'),
    );
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);

    // Instantiate the destination class using the node type.
    $this->destination = new MigrateDestinationTerm('iati_roles');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'code' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'code',
          'alias' => 'c',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('description', 'description');
    $this->addFieldMapping('field_iati_code', 'code');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMTermOrgRole' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }
}
