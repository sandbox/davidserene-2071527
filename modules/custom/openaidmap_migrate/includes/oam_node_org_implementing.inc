<?php

class OAMNodeOrgImplementing extends DynamicMigration {
  public function __construct($arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports organisations (implementing).');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
    
    $this->destination = new MigrateDestinationNode('iati_organisation');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sigla_entidad_ejecutora' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'sigla_entidad_ejecutora',
          'alias' => 'see',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'entidad_entidad_ejecutora');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMNodeOrgImplementing' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }
}
