<?php

class OAMTermSectorsLevel2 extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports the second level sector codes.');
//    $this->dependencies = array('OAMTermSectorsLevel1');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array(
      'parent' => 'parent',
    );
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);

    // Instantiate the destination class using the node type.
    $this->destination = new MigrateDestinationTerm('iati_activity_sector');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sector_tipo_proy' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'sector_tipo_proy',
          'alias' => 'tdc',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('name', 'sector_tipo_proy');
    $this->addFieldMapping('parent', 'parent');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMTermSectorsLevel2' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepareRow($row) {
    if (!empty($row->sector_tipo_proy) && !empty($row->sector_subsector)) {
      $term = db_select(str_replace('2', '1', $this->getMap()->getMapTable()), 'mt')
        ->fields('mt', array('destid1'))
        ->condition('sourceid1', $row->sector_subsector)
        ->execute()
        ->fetchAssoc();
      if ($term === FALSE) {
        drupal_set_message('Could not find the sub-sectors. Please load them before loading the lowest level sectors.');
        return FALSE;
      }
      $row->parent = $term['destid1'];
    }
    else {
      drupal_set_message('There is lowest level sector provided.');
      return FALSE;
    }
  }
}
