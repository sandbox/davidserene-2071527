<?php

class OAMNodeActivity extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports activities.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
    
    $this->destination = new MigrateDestinationNode('iati_activity');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'codigo_sisin' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'codigo_sisin',
          'alias' => 'cs',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'proyecto_nombre');
    $this->addFieldMapping('field_other_identifier', 'codigo_sisin');
//    $this->addFieldMapping('iati_admin_boundaries', '');
//    $this->addFieldMapping('iati_activity_sector', '');
//    $this->addFieldMapping('field_iati_activity_budget', '');
//    $this->addFieldMapping('field_iati_location', '');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMNodeActivity' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepare($entity, $row) {
    $entity->field_iati_activity_description[LANGUAGE_NONE][0]['value'] = $row->descripcion_solucion . "\n\n" . $row->descripcion_problema . "\n\n" . $row->objetivo_especifico;

    $entity->field_iati_activity_planned_date[LANGUAGE_NONE][0]['value'] = substr(date('c', strtotime($row->fecha_inicio_estimada)), 0, 19);
    $entity->field_iati_activity_planned_date[LANGUAGE_NONE][0]['value2'] = substr(date('c', strtotime($row->fecha_fin_estimada)), 0, 19);
  }
}
