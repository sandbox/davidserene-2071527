<?php

class OAMTermAdminAreasLevel1 extends DynamicMigration {
  public function __construct($arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports Admin Boundaries on the first sub-national level.');
//    $this->dependencies = array('OAMTermAdminAreasLevel0');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array(
      'parent' => 'parent',
    );
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);

    // Instantiate the destination class using the node type.
    $this->destination = new MigrateDestinationTerm('iati_admin_boundaries');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'ter_departamento_codigo' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'ter_departamento_codigo',
          'alias' => 'tdc',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('name', 'ter_departamento');
    $this->addFieldMapping('description', 'ter_departamento_abrev');
    $this->addFieldMapping('parent', 'parent');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMTermAdminAreasLevel1' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepareRow($row) {
    if ($row->ter_departamento_codigo != 10) {
      $vocabulary = taxonomy_vocabulary_machine_name_load('iati_admin_boundaries');
      $terms = taxonomy_get_tree($vocabulary->vid, 0, 1, FALSE);
      if ($terms === array()) {
        drupal_set_message('Could not find the country level Admin Area. Please load it before loading the first sub-national level of Admin Areas.');
        return FALSE;
      }
      $row->parent = $terms[0]->tid;;
    }
    else {
      return FALSE;
    }
  }
}
