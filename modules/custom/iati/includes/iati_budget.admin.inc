<?php

/**
 * The class used for iati budget entities
 */
class IatiBudgetData extends Entity {
  var $name = "IATI Budget";

  public function __construct($values = array()) {
    parent::__construct($values, 'iati_budget');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'iati/budgets/' . $this->bid);
  }
}

class IatiBudgetDataController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a Budget - we first set up the values that are specific
   * to our model schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the model.
   *
   * @return
   *   A model object with all default fields initialized.
   */
  public function create(array $values = array()) {
    global $user;
    // Add values that are specific to our Model
    $values += array(
      'bid' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
      'period_start' => 0,
      'period_start_text' => '',
      'period_end_text' => '',
      'period_end' => 0,
      'value_amount' => 0,
      'value_date' => 0,
    );

    $model = parent::create($values);
    return $model;
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    drupal_set_message("This has not been implemented yet.");
  }
}

/**
 * UI controller for Task Type.
 */
class IatiBudgetUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Budget.';
    return $items;
  }

  public function overviewForm($form, &$form_state) {
    if (user_access('administer iati budget')) {
     drupal_goto('admin/config/iati/budget/edit');
    }
    else {
      drupal_goto('admin/config/iati/budget');
    }
    return $form;
  }
}

function iati_budget_form($form, &$form_state, $entity = NULL) {
  $form['value_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Value Amount'),
    '#default_value' => $entity->value_amount,
    '#required' => TRUE,
  );
  $form['value_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Value Date'),
    '#default_value' => $entity->value_date,
    '#required' => TRUE,
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-1:+0',      
  );
  $form['period_start'] = array(
    '#type' => 'date_popup',
    '#title' => t('Period Start'),
    '#default_value' => $entity->period_start,
    '#required' => TRUE,
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-0:+10',      
  );  
  $form['period_start_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Period Start Text'),
    '#default_value' => $entity->period_start_text,
    '#required' => TRUE,
  );
  $form['period_end'] = array(
    '#type' => 'date_popup',
    '#title' => t('Period End'),
    '#default_value' => $entity->period_end,
    '#required' => TRUE,
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-0:+10',      
  );
  $form['period_end_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Period End Text'),
    '#default_value' => $entity->period_end_text,
    '#required' => TRUE,
  ); 

  field_attach_form('iati_budget', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function iati_budget_form_submit(&$form, &$form_state) {
  $budget = entity_ui_form_submit_build_entity($form, $form_state);

  $budget->period_start = iati_convert_date_string_to_timestamp($budget->period_start, 'Y-m-d');
  $budget->period_end = iati_convert_date_string_to_timestamp($budget->period_end, 'Y-m-d');
  $budget->value_date = iati_convert_date_string_to_timestamp($budget->value_date, 'Y-m-d');
  // Save and go back.
  $budget->save();
  $form_state['redirect'] = 'admin/config/iati/budget/edit';
}

function iati_convert_date_string_to_timestamp($date_string, $date_format) {
  $date = date_create_from_format($date_format, $date_string);
  return $date->getTimestamp();
}
