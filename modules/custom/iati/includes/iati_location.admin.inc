<?php

/**
 * The class used for iati location entities
 */
class IatiLocationData extends Entity {
  var $name = "IATI Location";

  public function __construct($values = array()) {
    parent::__construct($values, 'iati_location');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'iati/location/' . $this->lid);
  }
}

class IatiLocationDataController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a Location - we first set up the values that are specific
   * to our model schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the model.
   *
   * @return
   *   A model object with all default fields initialized.
   */
  public function create(array $values = array()) {
    global $user;
    // Add values that are specific to our Model
    $values += array(
      'lid' => 0,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
      'name' => '',
      'description' => '',
    );

    $model = parent::create($values);
    return $model;
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    drupal_set_message("This has not been implemented yet.");
  }
}

/**
 * UI controller for Task Type.
 */
class IatiLocationUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Location.';
    return $items;
  }

  public function overviewForm($form, &$form_state) {
    if (user_access('administer iati Location')) {
      drupal_goto('admin/config/iati/location/edit');
    }
    else {
      drupal_goto('admin/config/iati/location');
    }
    return $form;
  }
}

function iati_location_form($form, &$form_state, $entity = NULL) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Location Name'),
    '#default_value' => $entity->name,
    '#weight' => 0,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Location Description'),
    '#default_value' => $entity->description,
    '#weight' => 1,
    '#required' => TRUE,
  );

  field_attach_form('iati_location', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function iati_location_form_submit(&$form, &$form_state) {
  $location = entity_ui_form_submit_build_entity($form, $form_state);

  $location->save();
  $form_state['redirect'] = 'admin/config/iati/location/edit';
}
