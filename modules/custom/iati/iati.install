<?php
/**
 * @file
* Install, update and uninstall functions for the iati module.
*/


/**
 * Implements hook_schema().
 */
function iati_schema() {
  $schema = array();

  $schema['iati_budget'] = array(
    'title' => 'IATI Budget',
    'description' => "This element represents the organisation's total planning
      budget for a calendar period (typically, but not necessarily,
      a year). This element may be repeated for each period
      described (normally, periods should be consecutive).",
    'fields' => array(
      'bid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Id of the budget.',
      ),
      'created' => array(
        'type' => 'int',
        'default' => 0,
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Datetime the budget was inserted in the database.',
      ),
      'changed' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'description' => 'Date the budget was changed.',
        'default' => 0,
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'description' => 'User created the budget.',
      ),
      'period_start' => array(
        'type' => 'int',
        'length' => 255,
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'The starting date for the budget period, in ISO 8601
          format (e.g. 2010-04-01 for 1 April 2010).  This element
          must be present.',
      ),
      'period_start_text' => array(
        'description' => 'Start period Text',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'period_end' => array(
        'type' => 'int',
        'length' => 255,
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'The ending date for the budget period, in ISO 8601
          format (e.g. 2011-03-31 for 31 March 2011). This
          element must be present.',
      ),
      'period_end_text' => array(
        'description' => 'End period Text',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'value_amount' => array(
        'type' => 'int',
        'default' => 0,
        'unsigned' => FALSE,
        'not null' => TRUE,
        'description' => "The total value of the organisation's aid budget for
          this period. This element is required.",
      ),
      'value_date' => array(
        'type' => 'int',
        'length' => 255,
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => "The date the ammount was disbursed, which will be
          used to calculate the exchange rates on that date",
      ),
    ),
    'primary key' => array('bid'),
  );

  $schema['iati_location'] = array(
    'title' => 'location',
    'fields' => array(
      'lid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Id of the location.',
      ),
      'created' => array(
        'type' => 'int',
        'default' => 0,
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Datetime the budget was inserted in the database.',
      ),
      'changed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Date the budget was changed.',
        'default' => 0,
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'description' => 'User created the budget.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' =>255,
        'not null' => TRUE,
        'description' => 'Name of location',
        'default' => '',
      ),
      'description' => array(
        'description' => 'Description of location',
        'type' => 'varchar',
        'length' => 255,
        'not null' => False,
        'default' => '',
      ),
    ),
    'primary key' => array('lid'),
  );

  return $schema;
}

function iati_install() {
  $field = array(
    'field_name' => 'field_iati_geofield',
    'type' => 'geofield',
    'module' => 'geofield',
    'entity_types' => array('iati_location'),
    'translatable' => TRUE,
  );
  field_create_field($field);

  $instance = array(
    'entity_type' => 'iati_location',
    'field_name' => 'field_iati_geofield',
    'bundle' => 'iati_location',
    'label' => 'Iati Location',
    'widget' => array('type'=>'geofield_latlon',),
    'settings' => array('display_summary' => TRUE),
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'type' => 'geofield_latlon',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'type' => 'hidden',
      ),
    ),
  );
  field_create_instance($instance);

  $vocabulary = taxonomy_vocabulary_machine_name_load('iati_admin_boundaries');
  $field = array(
    'field_name' => 'field_' . $vocabulary->machine_name,
    'type' => 'taxonomy_term_reference',
    // Set cardinality to unlimited for tagging.
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $vocabulary->machine_name,
          'parent' => 0,
        ),
      ),
    ),
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_' . $vocabulary->machine_name,
    'entity_type' => 'iati_location',
    'label' => $vocabulary->name,
    'bundle' => 'iati_location',
    'description' => $vocabulary->description,
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => -4,
    ),
    'display' => array(
      'default' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
    ),
  );
  field_create_instance($instance);

  $field = array(
    'field_name' => 'field_iati_activity_ref',
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array('iati_activity')),
    ),
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_iati_activity_ref',
    'entity_type' => 'iati_location',
    'bundle' => 'iati_location',
    'label' => 'Activity',
    'widget' => array(
      'type' => 'entityreference_autocomplete',
    ),
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array('iati_activity')),
    ),
  );
  field_create_instance($instance);

  $instance = array(
    'field_name' => 'field_iati_activity_ref',
    'entity_type' => 'iati_budget',
    'bundle' => 'iati_budget',
    'label' => 'Activity',
    'widget' => array(
      'type' => 'entityreference_autocomplete',
    ),
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array('iati_activity')),
    ),
  );
  field_create_instance($instance);
}

function iati_uninstall() {
  db_drop_table('iati_budget');
  db_drop_table('iati_location');

  db_drop_table('field_data_field_iati_geofield');
  db_drop_table('field_revision_field_iati_geofield');
  db_query("delete from field_config where field_name='field_iati_geofield'");
  db_query("delete from field_config_instance where field_name='field_iati_geofield'");

  db_drop_table('field_data_field_iati_admin_boundaries');
  db_drop_table('field_revision_field_iati_admin_boundaries');
  db_query("delete from field_config where field_name='field_iati_admin_boundaries'");
  db_query("delete from field_config_instance where field_name='field_iati_admin_boundaries'");

  db_drop_table('field_data_field_iati_activity_ref');
  db_drop_table('field_revision_field_iati_activity_ref');
  db_query("delete from field_config where field_name='field_iati_activity_ref'");
  db_query("delete from field_config_instance where field_name='field_iati_activity_ref'");
}
