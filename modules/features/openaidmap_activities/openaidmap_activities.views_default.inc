<?php
/**
 * @file
 * openaidmap_activities.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function openaidmap_activities_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openaidmap_activities';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_om_main_activities';
  $view->human_name = 'OpenAidMap Activities';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nid' => 'nid',
    'field_organisation_name' => 'field_organisation_name',
    'relation_iati_organisation_role_node_reverse' => 'relation_iati_organisation_role_node_reverse',
    'relation_iati_organisation_role_node' => 'relation_iati_organisation_role_node',
    'relation_iati_parent_activity_node' => 'relation_iati_parent_activity_node',
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'nid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_organisation_name' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'relation_iati_organisation_role_node_reverse' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'relation_iati_organisation_role_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'relation_iati_parent_activity_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_om_main_activities';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'NAME OF ACTIVITY';
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: Relation iati_organisation_role (to node reverse) */
  $handler->display->display_options['fields']['relation_iati_organisation_role_node_reverse']['id'] = 'relation_iati_organisation_role_node_reverse';
  $handler->display->display_options['fields']['relation_iati_organisation_role_node_reverse']['table'] = 'search_api_index_om_main_activities';
  $handler->display->display_options['fields']['relation_iati_organisation_role_node_reverse']['field'] = 'relation_iati_organisation_role_node_reverse';
  $handler->display->display_options['fields']['relation_iati_organisation_role_node_reverse']['label'] = 'DONORS(still pending)';
  $handler->display->display_options['fields']['relation_iati_organisation_role_node_reverse']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['relation_iati_organisation_role_node_reverse']['view_mode'] = 'full';
  /* Field: Indexed Node: Sector */
  $handler->display->display_options['fields']['iati_activity_sector']['id'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['table'] = 'search_api_index_om_main_activities';
  $handler->display->display_options['fields']['iati_activity_sector']['field'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['label'] = 'SECTOR';
  $handler->display->display_options['fields']['iati_activity_sector']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['iati_activity_sector']['view_mode'] = 'full';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'activities';
  $translatables['openaidmap_activities'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('NAME OF ACTIVITY'),
    t('DONORS(still pending)'),
    t('SECTOR'),
    t('Page'),
  );
  $export['openaidmap_activities'] = $view;

  return $export;
}
