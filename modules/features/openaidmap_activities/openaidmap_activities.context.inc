<?php
/**
 * @file
 * openaidmap_activities.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function openaidmap_activities_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'projects_list_page';
  $context->description = '';
  $context->tag = 'OpenAidMap';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'openaidmap_activities' => 'openaidmap_activities',
        'openaidmap_activities:page' => 'openaidmap_activities:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'facetapi-RI7ZVOQk9Hg0CPfK2FuiyBa01u2NeECn' => array(
          'module' => 'facetapi',
          'delta' => 'RI7ZVOQk9Hg0CPfK2FuiyBa01u2NeECn',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'facetapi-DVyS2TS7Wue2i4Jf2C2qJpipOeHrSDTq' => array(
          'module' => 'facetapi',
          'delta' => 'DVyS2TS7Wue2i4Jf2C2qJpipOeHrSDTq',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'facetapi-YEBZrb8VpTfcuUC3Tdl0iqfyKzCXDWg1' => array(
          'module' => 'facetapi',
          'delta' => 'YEBZrb8VpTfcuUC3Tdl0iqfyKzCXDWg1',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'header_first',
          'weight' => '-10',
        ),
        'menu-menu-map-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-map-menu',
          'region' => 'preface_first',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'list_view',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('OpenAidMap');
  $export['projects_list_page'] = $context;

  return $export;
}
