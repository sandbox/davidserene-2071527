<?php
/**
 * @file
 * openaidmap_map_feature.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function openaidmap_map_feature_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'openlayers_default_map';
  $strongarm->value = 'default';
  $export['openlayers_default_map'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'projects';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
