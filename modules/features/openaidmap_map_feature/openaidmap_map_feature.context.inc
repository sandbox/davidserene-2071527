<?php
/**
 * @file
 * openaidmap_map_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function openaidmap_map_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'projects_page';
  $context->description = '';
  $context->tag = 'OpenAidMap';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'projects' => 'projects',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'header_first',
          'weight' => '-10',
        ),
        'menu-menu-map-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-map-menu',
          'region' => 'preface_first',
          'weight' => '-9',
        ),
        'facetapi-0TMDsPobKcykVwDesB91UzZt1OaKCfhq' => array(
          'module' => 'facetapi',
          'delta' => '0TMDsPobKcykVwDesB91UzZt1OaKCfhq',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
        'facetapi-VHMUVa0fM8B0JTT1mtApKweXw34twxZL' => array(
          'module' => 'facetapi',
          'delta' => 'VHMUVa0fM8B0JTT1mtApKweXw34twxZL',
          'region' => 'postscript_second',
          'weight' => '-9',
        ),
        'openlayers_plus-blockswitcher' => array(
          'module' => 'openlayers_plus',
          'delta' => 'blockswitcher',
          'region' => 'postscript_second',
          'weight' => '-8',
        ),
        'facetapi-pYXdF2elz4np1TuuAAVIsNCLZ8qqHR2h' => array(
          'module' => 'facetapi',
          'delta' => 'pYXdF2elz4np1TuuAAVIsNCLZ8qqHR2h',
          'region' => 'postscript_second',
          'weight' => '-6',
        ),
        'views-openaidmap_miscellaneous_-block' => array(
          'module' => 'views',
          'delta' => 'openaidmap_miscellaneous_-block',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('OpenAidMap');
  $export['projects_page'] = $context;

  return $export;
}
