<?php
/**
 * @file
 * openaidmap_map_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function openaidmap_map_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openaidmap_miscellaneous_';
  $view->description = 'This view has small things that still need to be exported. Miscellaneous ';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'OpenAidMap Miscellaneous ';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Footer */
  $handler = $view->new_display('block', 'Footer', 'block');
  $handler->display->display_options['display_description'] = 'Some text in the footer';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '© 2013 Open Aid Partnership. Todos los derechos reservados. Legal.';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $translatables['openaidmap_miscellaneous_'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Custom text'),
    t('Footer'),
    t('Some text in the footer'),
    t('© 2013 Open Aid Partnership. Todos los derechos reservados. Legal.'),
  );
  $export['openaidmap_miscellaneous_'] = $view;

  $view = new view();
  $view->name = 'projects';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_oam_activities';
  $view->human_name = 'Projects';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['search_api_bypass_access'] = 0;
  $handler->display->display_options['query']['options']['entity_access'] = 0;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'openlayers_map';
  $handler->display->display_options['style_options']['map'] = 'openaidmap_activitiesmap';
  /* Field: Indexed IATI Location: Iati location ID */
  $handler->display->display_options['fields']['lid']['id'] = 'lid';
  $handler->display->display_options['fields']['lid']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['lid']['field'] = 'lid';
  /* Field: Indexed IATI Location: Label */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Location';
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Iati Location » Latitude */
  $handler->display->display_options['fields']['field_iati_geofield_lat']['id'] = 'field_iati_geofield_lat';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['field'] = 'field_iati_geofield_lat';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['label'] = 'Latitude';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['precision'] = '0';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['separator'] = '';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Iati Location » Longitude */
  $handler->display->display_options['fields']['field_iati_geofield_lon']['id'] = 'field_iati_geofield_lon';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['field'] = 'field_iati_geofield_lon';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['label'] = 'Longitude';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['precision'] = '0';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['separator'] = '';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Activity */
  $handler->display->display_options['fields']['field_iati_activity_ref']['id'] = 'field_iati_activity_ref';
  $handler->display->display_options['fields']['field_iati_activity_ref']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['field_iati_activity_ref']['field'] = 'field_iati_activity_ref';
  $handler->display->display_options['fields']['field_iati_activity_ref']['label'] = 'Activities';
  $handler->display->display_options['fields']['field_iati_activity_ref']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['field_iati_activity_ref']['view_mode'] = 'full';

  /* Display: Map Page */
  $handler = $view->new_display('page', 'Map Page', 'page');
  $handler->display->display_options['display_description'] = 'The display for the main map';
  $handler->display->display_options['path'] = 'projects';

  /* Display: Projects Data Overlay */
  $handler = $view->new_display('openlayers', 'Projects Data Overlay', 'openlayers_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_data';
  $handler->display->display_options['style_options']['data_source'] = array(
    'value' => 'other_latlon',
    'other_lat' => 'field_iati_geofield_lat',
    'other_lon' => 'field_iati_geofield_lon',
    'wkt' => 'lid',
    'other_top' => 'lid',
    'other_right' => 'lid',
    'other_bottom' => 'lid',
    'other_left' => 'lid',
    'name_field' => 'name',
    'description_field' => '',
    'style_field' => '',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed IATI Location: Iati location ID */
  $handler->display->display_options['fields']['lid']['id'] = 'lid';
  $handler->display->display_options['fields']['lid']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['lid']['field'] = 'lid';
  $handler->display->display_options['fields']['lid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['lid']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Label */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Location';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['label'] = '';
  $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['description']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Iati Location » Latitude */
  $handler->display->display_options['fields']['field_iati_geofield_lat']['id'] = 'field_iati_geofield_lat';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['field'] = 'field_iati_geofield_lat';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['label'] = 'Latitude';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['precision'] = '0';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['separator'] = '';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['link_to_entity'] = 0;
  /* Field: Indexed IATI Location: Iati Location » Longitude */
  $handler->display->display_options['fields']['field_iati_geofield_lon']['id'] = 'field_iati_geofield_lon';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['field'] = 'field_iati_geofield_lon';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['label'] = 'Longitude';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['precision'] = '0';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['separator'] = '';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['link_to_entity'] = 0;
  /* Field: Activity: Sector (indexed) */
  $handler->display->display_options['fields']['field_iati_activity_ref_iati_activity_sector']['id'] = 'field_iati_activity_ref_iati_activity_sector';
  $handler->display->display_options['fields']['field_iati_activity_ref_iati_activity_sector']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['field_iati_activity_ref_iati_activity_sector']['field'] = 'field_iati_activity_ref_iati_activity_sector';
  $handler->display->display_options['fields']['field_iati_activity_ref_iati_activity_sector']['label'] = 'Sectors';
  $handler->display->display_options['fields']['field_iati_activity_ref_iati_activity_sector']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['field_iati_activity_ref_iati_activity_sector']['view_mode'] = 'full';
  /* Field: Indexed IATI Location: Activity */
  $handler->display->display_options['fields']['field_iati_activity_ref']['id'] = 'field_iati_activity_ref';
  $handler->display->display_options['fields']['field_iati_activity_ref']['table'] = 'search_api_index_oam_activities';
  $handler->display->display_options['fields']['field_iati_activity_ref']['field'] = 'field_iati_activity_ref';
  $handler->display->display_options['fields']['field_iati_activity_ref']['label'] = 'Activities';
  $handler->display->display_options['fields']['field_iati_activity_ref']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['field_iati_activity_ref']['view_mode'] = 'full';
  $translatables['projects'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Iati location ID'),
    t('.'),
    t(','),
    t('Location'),
    t('Description'),
    t('Latitude'),
    t('Longitude'),
    t('Activities'),
    t('Map Page'),
    t('The display for the main map'),
    t('Projects Data Overlay'),
    t('Sectors'),
  );
  $export['projects'] = $view;

  return $export;
}
