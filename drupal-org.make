; Drupal.org release file.
core = 7.x
api = 2

; Basic contributed modules.
projects[context][version] = 3
projects[context][type] = "module"
projects[context][subdir] = "contrib"

projects[ctools][version] = 1.3
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[entity][version] = 1.1
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[entityreference][version] = 1.0
projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"

projects[cer][version] = 2.x-dev
projects[cer][type] = "module"
projects[cer][subdir] = "contrib"

projects[relation_add][version] = 1.1
projects[relation_add][type] = "module"
projects[relation_add][subdir] = "contrib"

projects[relation][version] = 1.0-rc4
projects[relation][type] = "module"
projects[relation][subdir] = "contrib"
projects[relation][patch][] = "https://drupal.org/files/relation_sources_targets.patch"

projects[views][version] = 3.7
projects[views][type] = "module"
projects[views][subdir] = "contrib"

projects[views_infinite_scroll][version] = 1.1
projects[views_infinite_scroll][type] = "module"
projects[views_infinite_scroll][subdir] = "contrib"

projects[features][subdir] = contrib
projects[features][type] = "module"
projects[features][version] = 2.0-rc1

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[libraries][version] = 2.1
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

; Other contribs.
projects[inline_entity_form][version] = 1.2
projects[inline_entity_form][type] = "module"
projects[inline_entity_form][subdir] = "contrib"

projects[advanced_help][version] = 1.x-dev
projects[advanced_help][type] = "module"
projects[advanced_help][subdir] = "contrib"

projects[token][version] = 1.5
projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[token][patch][] = "http://drupal.org/files/token-token_asort_tokens-1712336_0.patch"

projects[migrate][version] = 2.5
projects[migrate][type] = "module"
projects[migrate][subdir] = "contrib"

projects[migrate_extras][version] = 2.5
projects[migrate_extras][type] = "module"
projects[migrate_extras][subdir] = "contrib"

projects[date][version] = 2.6
projects[date][type] = "module"
projects[date][subdir] = "contrib"

;Search related modules.
projects[search_api][subdir] = contrib
projects[search_api][type] = "module"
projects[search_api][version] = 1.6

projects[search_api_db][version] = 1.0-rc1
projects[search_api_db][type] = "module"
projects[search_api_db][subdir] = "contrib"

projects[search_api_solr][version] = 1.x-dev
projects[search_api_solr][type] = "module"
projects[search_api_solr][subdir] = "contrib"

projects[search_api_ranges][version] = 1.4
projects[search_api_ranges][type] = "module"
projects[search_api_ranges][subdir] = "contrib"

projects[facetapi][version] = 1.3
projects[facetapi][type] = "module"
projects[facetapi][subdir] = "contrib"
projects[facetapi][patch][] = "http://drupal.org/files/facetapi-1616518-13-show-active-term.patch"
projects[facetapi][patch][] = "http://drupal.org/files/1665164-facetapi-override_facet_label-7.patch"

projects[search_api_sorts][version] = 1.4
projects[search_api_sorts][type] = "module"
projects[search_api_sorts][subdir] = "contrib"

projects[purl][version] = 1.x-dev
projects[purl][type] = "module"
projects[purl][subdir] = "contrib"

projects[purl_search_api][version] = 1.0
projects[purl_search_api][type] = "module"
projects[purl_search_api][subdir] = "contrib"

projects[views_modes][version] = 1.x-dev
projects[views_modes][type] = "module"
projects[views_modes][subdir] = "contrib"

; Graphing

projects[charts_graphs][version] = 2.0
projects[charts_graphs][type] = "module"
projects[charts_graphs][subdir] = "contrib"

projects[charts_graphs_flot][version] = 1.x-dev
projects[charts_graphs_flot][type] = "module"
projects[charts_graphs_flot][subdir] = "contrib"

projects[facetapi_graphs][version] = 1.x-dev
projects[facetapi_graphs][type] = "module"
projects[facetapi_graphs][subdir] = "contrib"
;projects[facetapi_graphs][download][type] = git
;projects[facetapi_graphs][download][url] = "http://git.drupal.org/sandbox/batje/1570344.git"
;projects[facetapi_graphs][download][revision] = "be3c416c42c2be8200f9634211378bb0a73f8926"
;projects[openlayers_plus][patch] = "https://drupal.org/files/openlayers_plus-Fixed-the-pagers-2033795-8.patch"

projects[flot][version] = 1.x-dev
projects[flot][type] = "module"
projects[flot][subdir] = "contrib"

projects[d3][version] = 1.x-dev
projects[d3][type] = "module"
projects[d3][subdir] = "contrib"
projects[d3][download][type] = git
projects[d3][download][url] = "http://git.drupal.org/sandbox/asherry/1477334.git"

projects[charts_graphs_d3][version] = 1.x-dev
projects[charts_graphs_d3][type] = "module"
projects[charts_graphs_d3][subdir] = "contrib"
projects[charts_graphs_d3][download][type] = git
projects[charts_graphs_d3][download][url] = "http://git.drupal.org/sandbox/ssekono/2028575.git"

; UI improvement modules.
projects[module_filter][version] = 1.7
projects[module_filter][subdir] = "contrib"

projects[link][version] = 1.1
projects[link][type] = "module"
projects[link][subdir] = "contrib"
projects[link][patch][] = "http://drupal.org/files/Fixed_title_value_in_link_field_update_instance_undefined-1914286-3.patch"
projects[link][patch][] = "http://drupal.org/files/link-fix-undefined-index-widget-1918850-9.patch"

projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"

; Internationalization
projects[variable][version] = 2.2
projects[variable][type] = "module"
projects[variable][subdir] = "contrib"

projects[i18n][version] = "1.x-dev"
projects[i18n][type] = "module"
projects[i18n][subdir] = "contrib"

; Base theme.
projects[omega][version] = 3.1
projects[omega][type] = "theme"

projects[omega_tools][version] = 3.0-rc4
projects[omega_tools][subdir] = "contrib"
projects[omega_tools][type] = "module"

projects[delta][version] = 3.0-beta11
projects[delta][subdir] = "contrib"
projects[delta][type] = "module"

projects[shiny][version] = 1.2
projects[shiny][type] = "theme"

; Mapping
projects[openlayers][version] = 2.0-beta7
projects[openlayers][type] = "module"
projects[openlayers][subdir] = "contrib"

projects[geofield][version] = 2.0-alpha2
projects[geofield][type] = "module"
projects[geofield][subdir] = "contrib"
projects[geofield][patch][] = "http://drupalcode.org/project/geofield.git/patch/7b5c2b6"

projects[openlayers_plus][version] = 2.x-dev
projects[openlayers_plus][type] = "module"
projects[openlayers_plus][subdir] = "contrib"
;projects[openlayers_plus][download][type] = "git"
;projects[openlayers_plus][download][revision] = "4b76fb1631f8fa0b7b1d34f820ff783b5c2412f0"

projects[geophp][version] = 1.7
projects[geophp][type] = "module"
projects[geophp][subdir] = "contrib"

projects[proj4js][version] = 1.2
projects[proj4js][type] = "module"
projects[proj4js][subdir] = "contrib"

projects[views_rss][version] = 2.0-rc3
projects[views_rss][type] = "module"
projects[views_rss][subdir] = "contrib"

projects[kml][version] = 1.0-alpha1
projects[kml][type] = "module"
projects[kml][subdir] = "contrib"
projects[kml][patch][] = "http://drupal.org/files/kml_blockify.patch" 

projects[views_data_export][version] = 3.0-beta6
projects[views_data_export][type] = "module"
projects[views_data_export][subdir] = "contrib"
projects[views_data_export][patch][] = "http://drupal.org/files/views_data_export_blockify.patch"

projects[blockify][version] = 1.2
projects[blockify][type] = "module"
projects[blockify][subdir] = "contrib"


; Quick stuff
projects[quicktabs][version] = 3
projects[quicktabs][type] = "module"
projects[quicktabs][subdir] = "contrib"

projects[media][version] = 1
projects[media][type] = "module"
projects[media][subdir] = "contrib"

projects[admin_menu][version] = 3
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[diff][version] = 3
projects[diff][type] = "module"
projects[diff][subdir] = "contrib"

projects[module_filter][subdir] = contrib
projects[module_filter][type] = "module"
projects[module_filter][version] = 1.7

; Libraries.

libraries[flot][download][type] = "file"
libraries[flot][download][url] = "http://flot.googlecode.com/files/flot-0.7.tar.gz"
libraries[flot][download][sha1] = "68ca8b250d18203ebe67136913e0e1c82bbeecfb"
libraries[flot][destination] = "libraries"

libraries[autopager][download][type] = "file"
libraries[autopager][download][url] = "http://jquery-autopager.googlecode.com/files/jquery.autopager-1.0.0.js"
libraries[autopager][download][sha1] = "3125f05ff4cd2471730f0775ddee03ea141988cd"
libraries[autopager][destination] = "libraries" 
