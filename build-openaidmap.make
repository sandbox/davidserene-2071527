api = 2
core = 7.x
projects[drupal][version] = 7.22
; Include the definition for how to build Drupal core directly, including patches:
projects[openaidmap][type] = "profile"
projects[openaidmap][download][type] = "git"
projects[openaidmap][download][branch] = "7.x-1.x"
projects[openaidmap][download][url]  = "http://git.drupal.org/sandbox/batje/2012474.git"
